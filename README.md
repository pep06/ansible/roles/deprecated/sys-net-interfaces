# Ansible role ``sys_net_interfaces``

> Unified network interface configuration for mixed environments.

## Overview
This role allows to configure network interfaces in an (almost)
platform agnostic manner. It defines generic data structures to
represent various aspects of a network configuration, and translates
them to native configuration files for different network frameworks,
such as _ifupdown_ or _freebsd_rc_.

This role is very complex, and as a consequence is divided in several
sub-roles:

| Sub-role                    | Description                  |
| --------------------------- | ---------------------------- |
| ``sys/net/interfaces.init`` | Performs role initialization |
| ``sys/net/interfaces``      | Main entry point             |

### Roadmap
The following tasks are pending implementation, by decreasing order
of importance:

  - Finalize support for FreeBSD rc network framework: inet6 aliases
  - Move default gateway support to a separate role.

### Interfaces types
Each interface configured by this role must be assigned a type. This
section documents all supported interface types.

#### ``adapter`` interfaces
The most basic type of interface is the **adapter** interface. It is
techically the equivalent of a physical interface, but we decided to
use another word than _physical_ because this category of interfaces
may also include _virtual_ or _emulated_ ones.

#### ``bond`` interfaces
Bond interfaces are used to implement network bonding, to provide link
failover or aggregation. These interfaces are always virtual, and
are linked to one or more slave ``adapter`` interfaces.

#### ``bridge`` interfaces
Bridge interfaces are used implement network bridge. These interfaces
are always virtual, and are linked to zero or more member interfaces,
which may be of any type.

#### ``vlan`` interfaces
vLAN interfaces are used to filter network packets by vLAN ID. These
interfaces are always virtual, and must be bound to another interface,
of any type.

### IPv6 features
Due to the complexity of the IPv6 protocol stack, we deem useful to
add a few remarks here about how its different parts of fit together.
The following truth table shows how interfaces might be configured
to achieve various IPv6 addressing modes:

| Addressing mode          | ``inet6.slaac.enabled`` | ``inet6.dhcp.enabled`` |
| ------------------------ | ----------------------- | ---------------------- |
| No autoconfiguration     | ``false``               | ``false``              |
| SLAAC only               | ``true``                | ``false``              |
| SLAAC + stateless DHCPv6 | ``true``                | ``true``               |
| Stateful DHCPv6          | ``false``               | ``true``               |

## Role variables
This section documents inventory variables used by this role.

### ``my_sys_net_interfaces`` (**dict[]**, _optional_)
#### Description
This variable holds a list of network interfaces to configure. Default
value is the empty list, which makes the role do nothing. You must
supply at least one list entry to use the role in a productive way.

#### Default value
This variable defaults to an empty ``list``.

#### Expected values
Each entry of this list is a _dict_ data structure representing a single
network interface. Interfaces entries may become quite complex. The
expected data structure is detailed in the next section.

### ``my_sys_net_interface_framework`` (**str**, _optional_)
#### Description
This variable holds the name of the network framework to use to handle
the network configuration of the node. By default, this variable set to
``None``, which lets the role decide which network framework to use.
Set it to a non-null value force the use a specific network framework
instead of the default one.

#### Default value
This variable defaults to ``None``. However, the role includes
built-in defaults for every supported platform, which are exposed in the
following table:

| Platform | Default framework | Supported frameworks |
| -------- | ----------------- | -------------------- |
| Debian   | ``ifupfown``      | ``ifupfown``         |
| FreeBSD  | ``freebsd_rc``    | ``freebsd_rc``       |

#### Expected values
Expected values are names of supported networks frameworks:

| Value          | Description                                                    |
| -------------- | -------------------------------------------------------------- |
| ``ifupdown``   | The _ifupdown_ framework, mainly used on Debian nodes          |
| ``freebsd_rc`` | FreeBSD network configuration framework, based on rc variables |

## Structure of ``my_sys_net_interfaces[item]``
This section documents the expected data structure of a _dict_
representing a single network interface. One or several of such _dict_
are supplied to the role as entries of the ``my_sys_net_interfaces``
list variable.

The full reference of all supported keys is represented below:

````yaml
#-----------------------------------------------------------------------
# Basic information
#-----------------------------------------------------------------------
uid: <str>              # Unique internal interface identifier
desc: <str>             # Interface description
name: <str>             # Interface custom name
type: <str>             # Interface type
renamed: <bool>         # Whether to rename the interface

# Automatic interface startup options
auto_start:
  on_boot: <bool>       # Whether to autostart interface on boot
  on_plug: <bool>       # Whether to autostart interface on hotplug

#-----------------------------------------------------------------------
# Options for 'adapter' interfaces
#-----------------------------------------------------------------------
adapter:
  device: <str>         # Name of the physical device of the interface

#-----------------------------------------------------------------------
# Options for 'bond' interfaces
#-----------------------------------------------------------------------
bond:
  hash_policy: <str>    # Transmit hash policy (with supported modes).
  index: <int>          # Mandatory bond device index (0-2048)
  mode: <str>           # Bonding mode. See doc. for legal values.
  slaves: <str[]>       # List of slave adapter interfaces

#-----------------------------------------------------------------------
# Options for bridge interfaces
#-----------------------------------------------------------------------
bridge:
  index: <int>          # Mandatory bridge device index (0-2048)
  members: <str[]>      # List of bridge member interfaces
  # Spanning Tree Protocol options
  stp:
    enabled: <bool>     # Whether to enable STP on the bridge interface
    fwd_delay: <int>    # Delay before traffic forwarding
    hello_time: <int>   # Delay between STP configuration messages
    priority: <int>     # Bridge STP priority

#-----------------------------------------------------------------------
# Options for 'vlan' interfaces
#-----------------------------------------------------------------------
vlan:
  id: <int>             # Mandatory device index and vLAN ID (0-4096)
  parent: <str>         # Parent interface

#-----------------------------------------------------------------------
# Ethernet options
#-----------------------------------------------------------------------
ethernet:
  address: <str>        # Custom hardware MAC address (for MAC spoofing)
  randomized: <bool>    # Whether to use a random hardware address
  mtu: <int>            # Custom MTU size

#-----------------------------------------------------------------------
# IPv4 options
#-----------------------------------------------------------------------
inet4:
  enabled: <bool>       # Whether to enable the IPv4 protocol stack

  # Address Resolution Protocol options
  arp:
    enabled: <bool>     # Whether to enable ARP on this interface

  # BOOTP options
  bootp:
    enabled: <bool>     # Whether to enable BOOTP on this interface
    file: <str>         # Optional file hint to pass to the server
    server: <str>       # Optional static BOOTP server address

  # Dynamic Host Control Protocol options
  dhcp:
    enabled: <bool>     # Whether to enable DHCPv4 on this interface.

  # Static IPv4 addressing
  static:
    - address: <str>    # Static address in dotted decimal notation
      broadcast: <str>  # Optional explicit broadcast address
      length:  <int>    # Prefix length (CIDR notation)
      # Optional default gateway information
      gateway:
        address: <str>  # GW address in dotted decimal notation
        metric: <int>   # Routing metric for the default gateway
    # (...)

#-----------------------------------------------------------------------
# IPv6 options
#-----------------------------------------------------------------------
inet6:
  enabled: <bool>       # Whether to enable the IPv6 protocol stack

  # Configuration of the Network Discovery Protocol
  ndp:
    # Duplicate Address Detection options
    dad:
      enabled:  <bool>  # Whether to enable DAD
      attempts: <int>   # Number of attempts to settle DAD
      interval: <int>   # DAD state polling interval in seconds
    # Neighbout Unreachability Detection
    nud:
      enabled: <bool>   # Whether to enable NUD
    # Router Advertisement / Solicitation options
    rtadv:
      enabled: <bool>   # Whether to enable RA/RS messages
      forced:  <bool>   # Whether to enable RA/RS messages on routers

  # Configuration of StateLess Address AutoConfiguration
  slaac:
    enabled: <bool>     # Whether to enable/disable SLAAC entirely
    # RFC 3041 / 4941 addresses (privacy extensions)
    privacy:
      enabled:   <bool> # Whether to enable temporary addresses
      preferred: <bool> # Whether to prefer temporary addresses
    # Link local address autoconfiguration
    link_local:
      attempts: <int>   # Number of attempts to get a link-local addr
      enabled:  <bool>  # Whether to autoconfigure link local addresses
      interval: <int>   # Link-local addr polling interval in seconds

  # Configuration of DHCPv6
  dhcp:
    enabled: <bool>     # Whether to enable DHCPv6.
    prefix:             # Prefix options
      requested: <bool> # Whether to request an IPv6 network prefix

  # Static IPv6 addressing
  static:
    - address: <str>    # Static address in colon'd hexa notation
      anycast: <bool>   # Whether the address is unicast or anycast
      length:  <int>    # Prefix length of the static address
      # Optional default gateway information
      gateway:
        address: <str>  # GW address in colon'd hexa notation
        metric: <int>   # Routing metric for the default gateway
    # (...)
````

Next sections will document each key of the dictionary.

### ``uid`` (**str**, _mandatory_, all types)
#### Description
The mandatory ``uid`` key assigns a unique internal identifier to the
represented interface. This uid allows the interface to be referenced
from other contexts without relying on an unpredictable device name.

#### Support status
This key is only used internally by the role, and is supported in every
context.

#### Expected values
A single word, without space nor special characters: ``/[a-zA-Z0-9_]+/``

### ``desc`` (**str**, _optional_, all types)
#### Description
The optional ``desc`` key may hold a short description of the interface.

#### Support status
The following table shows how this setting shall be rendered on
supported network frameworks:

| Framework      | Value | Rendering                                                            |
| -------------- | ----- | -------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                            |
| **freebsd_rc** | Set   | ``ifconfig_<device>_descr="<desc>"`` var in ``/etc/rc.conf.d/netif`` |
| **ifupdown**   | Unset | _Nothing_                                                            |
| **ifupdown**   | Set   | Comments in the  ``/etc/network/interfaces`` file                    |

#### Expected values
A simple string shorter than or equal to 127 characters without special
chars. Longer strings shall be trimmed for technical reasons.

### ``name`` (**str**, _optional_, all types)
#### Description
The optional ``name`` allows to specify a custom name for the interface.
If the ``renamed`` key is also set to ``true``, the role will attempt
to rename the interface with the value set with this key.

#### Support status
This key is only used internally by the role, and is supported in every
context.

#### Expected values
A very short alphanumeric string without special chars.

### ``type`` (**str**, _mandatory_, all types)
#### Description
The mandatory ``type`` key defines the type of the interface which is
being represented.

#### Support status
This key is not rendered as a single item, but will be used in various
places all along the configuration process.

#### Expected values
This key accepts the following values:

| Value       | Description                                 |
| ----------- | ------------------------------------------- |
| ``adapter`` | Define the interface as an Ethernet adapter |
| ``bond``    | Define the interface as a virtual bond      |
| ``bridge``  | Define the interface as a virtual bridge    |

### ``enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``enabled`` key indicates whether the interface is enabled.
Disabled interfaces will be explicitly disabled on the remote node.

#### Support status
The following table shows how this setting shall be rendered with each
supported framework:

| Framework      | Value     | Rendering                                          |
| -------------- | --------- | -------------------------------------------------- |
| **freebsd_rc** | ``null``  | ``down`` flag in ``ifconfig_<device>`` rc variable |
| **freebsd_rc** | ``false`` | ``down`` flag in ``ifconfig_<device>`` rc variable |
| **freebsd_rc** | ``true``  | ``up`` flag in ``ifconfig_<device>`` rc variable   |
| **ifupdown**   | ``null``  | _Nothing_                                          |
| **ifupdown**   | ``false`` | _Nothing_                                          |
| **ifupdown**   | ``true``  | ``iface <name> *`` stanzas                         |

#### Expected values
This key accepts the following values:

| Value     | Description                              |
| --------- | ---------------------------------------- |
| ``false`` | Disable the interface on the remote node |
| ``true``  | Enable the interface on the remote node  |

### ``renamed`` (**bool**, _optional_, all types)
#### Description
The optional ``renamed`` key indicates whether the role should attempt
to rename the interface device with the value of the ``name`` property.

#### Support status
The following table shows how this setting shall be rendered with each
supported framework:

| Framework      | Value     | Rendering                                                           |
| -------------- | --------- | ------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                           |
| **freebsd_rc** | ``false`` | _Nothing_                                                           |
| **freebsd_rc** | ``true``  | ``ifconfig_<device>_name="<name>"`` var in ``/etc/rc.conf.d/netif`` |
| **ifupdown**   | ``null``  | _Nothing_                                                           |
| **ifupdown**   | ``false`` | _Nothing_                                                           |
| **ifupdown**   | ``true``  | Custom device names in ``iface <name> *`` stanzas                   |

Interface renaming is not supported for every interface, with every
framework or on every platform. The following matrix shows renaming
support status in various contexts:

| Platform    | Framework  | ``adapter``   | ``bond``  | ``bridge`` | ``vlan``  |
| ----------- | ---------- | ------------- | --------- | ---------- | --------- |
| **Debian**  | ifupdown   | Not supported | Supported | Supported  | Supported |
| **FreeBSD** | freebsd_rc | Supported     | Supported | Supported  | Supported |

#### Expected values
This key accepts the following values:

| Value     | Description                        |
| --------- | ---------------------------------- |
| ``false`` | Do not rename the interface device |
| ``true``  | Rename the interface device        |

### ``auto_start.on_boot`` (**bool**, _optional_, all types)
#### Description
The optional ``auto_start.on_boot`` key allows to specify whether the
interface should be started automatically at boot time.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                         |
| -------------- | --------- | ------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                         |
| **freebsd_rc** | ``false`` | ``NOAUTO`` keyword in ``ifconfig_<dev>`` variable |
| **freebsd_rc** | ``true``  | _Nothing_                                         |
| **ifupdown**   | ``null``  | _Nothing_                                         |
| **ifupdown**   | ``false`` | _Nothing_                                         |
| **ifupdown**   | ``true``  | ``allow-auto <device>`` stanza                    |

#### Expected values
This key accepts the following values:

| Value     | Description                            |
| --------- | -------------------------------------- |
| ``false`` | Enable automatic startup at boot time  |
| ``true``  | Disable automatic startup at boot time |

### ``auto_start.on_plug`` (**bool**, _optional_, all types)
#### Description
The optional ``auto_start.on_plug`` key allows to specify whether the
interface should be started automatically on device hot plug.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                         |
| -------------- | --------- | --------------------------------- |
| **freebsd_rc** | _Any_     | _Unsupported_                     |
| **ifupdown**   | ``null``  | _Nothing_                         |
| **ifupdown**   | ``false`` | _Nothing_                         |
| **ifupdown**   | ``true``  | ``allow-hotplug <device>`` stanza |

#### Expected values
This key accepts the following values:

| Value     | Description                          |
| --------- | ------------------------------------ |
| ``false`` | Enable automatic startup on hotplug  |
| ``true``  | Disable automatic startup on hotplug |

### ``adapter.device`` (**str**, _mandatory_, adapter)
#### Description
The mandatory ``adapter.device`` key allows to specify the literal
name of the backing device of an adapter interface. Adapter interfaces
are the only type of interfaces requiring a literal device name. This is
needed because Linux and FreeBSD have very different ways to name
network interfaces, and the role cannot build their name
programmatically.

#### Support status
This key is not rendered as a single item, but will be used in various
places all along the configuration process.

#### Expected values
Any valid device name, such as ``eno1`` or ``igb3``.

### ``bond.hash_policy`` (**str**, _optional_, bond)
#### Description
The optional ``bond.hash_policy`` key allows to specify which data
sources should be used to compute hash values then used to load balance
traffic. The hash policy in only relevant with bond interfaces using an
operation mode based on hashes: such modes are ``hash``, ``lacp``,
``tlbh`` and ``alb``.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                            |
| -------------- | ----- | -------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                            |
| **freebsd_rc** | Set   | ``lagghash <policy>`` option in ``create_args_<device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                            |
| **ifupdown**   | Set   | ``bond_xmit_hash_policy <policy>`` option in ``inet manual`` stanza  |

While this role supports an extensive set of hash policies, target
platforms usually only implement a subset of them. As a consequence,
when the specified policy is not supported on the target platform, the
role will fall back to the closest policy available. In particular, when
the requested hash policy uses data from several OSI layers, the role
will fallback to the closest policy to the highest layer. The following
table maps policy names to their native counterparts on each platform:

| Policy       | Linux                                 | FreeBSD                            |
| ------------ | ------------------------------------- | ---------------------------------- |
| ``layer2``   | ``layer2``                            | ``l2``                             |
| ``layer3``   | Unsupported. Fallback to ``layer2+3`` | ``l3``                             |
| ``layer23``  | ``layer2+3``                          | ``l2,l3``                          |
| ``layer4``   | Unsupported. Fallback to ``layer3+4`` | ``l4``                             |
| ``layer24``  | Unsupported. Fallback to ``layer3+4`` | ``l2,l4``                          |
| ``layer34``  | ``layer3+4``                          | ``l3,l4``                          |
| ``layer234`` | Unsupported. Fallback to ``layer3+4`` | ``l2,l3,l4``                       |
| ``encap23``  | ``encap2+3``                          | Unsupported. Fallback to ``l2,l3`` |
| ``encap34``  | ``encap3+4``                          | Unsupported. Fallback to ``l3,l4`` |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``layer2``    |
| **FreeBSD** | ``layer234``  |

#### Expected values
The following table lists legal values for this key. The **802.3ad**
column shows whether the policy is 802.3ad compliant.

| Value         | Description                                               | 802.3ad |
| ------------- | --------------------------------------------------------- | ------- |
| ``"layer2"``  | Use information from layer 2 to generate hashes           | yes     |
| ``"layer23"`` | Use information from layers 2 and 3 to generate hashes    | yes     |
| ``"layer3"``  | Use information from layer 3 to generate hashes           | yes     |
| ``"layer34"`` | Use information from layers 3 and 4 to generate hashes    | yes     |
| ``"layer4"``  | Use information from layer 4 to generate hashes           | yes     |
| ``"encap23"`` | Same as ``layer23`` with support for encapsulated headers | no      |
| ``"encap34"`` | Same as ``layer34`` with support for encapsulated headers | no      |

### ``bond.index`` (**int**, _mandatory_, bond)
#### Description
The mandatory key ``bond.index`` allows to specify a literal index
number for a bond interface. The role needs this number to build the
dynamic device name of the interface.

#### Support status
This key is only used internally by the role, and is supported in every
context.

#### Expected values
Any integer in the range 0 - 2048.

### ``bond.mode`` (**str**, _optional_, bond)
#### Description
The optional ``bond.mode`` key allows to define the operation mode
of a bond interface. Supported operation modes depend on the target
platform. See the _Expected values_ section for more info about them.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                 |
| -------------- | ----- | ------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                 |
| **freebsd_rc** | Set   | ``laggproto <local_mode>`` option in ``create_args_<device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                 |
| **ifupdown**   | Set   | ``bond_mode <local_mode>`` option in ``inet manual`` stanza               |

While this role supports an extensive set of bonding modes, target
platforms usually only implement a subset of them. As a consequence,
when the specified mode is not supported on the target platform, the
role will fall back to the most similar mode available. The following
table maps mode names to their native counterparts on each platform:

| Mode           | Linux                     | FreeBSD                                  |
| -------------- | ------------------------- | ---------------------------------------- |
| ``broadcast``  | ``broadcast``             | ``broadcast``                            |
| ``failover``   | ``active-backup ``        | ``failover``                             |
| ``hash``       | ``balance-xor``           | ``loadbalance``                          |
| ``lacp``       | ``802.3ad``               | ``lacp``                                 |
| ``roundrobin`` | ``balance-rr ``           | ``roundrobin``                           |
| ``tlbd``       | ``balance-tlb`` (dynamic) | Unsupported. Fallback to ``roundrobin``  |
| ``tlbh``       | ``balance-tlb `` (hash)   | Unsupported. Fallback to ``loadbalance`` |
| ``alb``        | ``balance-alb``           | Unsupported  Fallback to ``roundrobin``  |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value  |
| ----------- | -------------- |
| **FreeBSD** | ``failover``   |
| **Debian**  | ``roundrobin`` |

#### Expected values
The following table lists legal values for this key. Special columns:
  - The **AS** column shows the number of active slaves at a time
  - The **FT** column shows whether the mode provides fault tolerance
  - The **TLB** and **RLB** columns show whether the mode provides
    transmit or receive load balancing, and the criteria used to do so.
  - The **SS** column shows whether the mode requires switch support.

| Value            | Description                              |  AS | FT  | TLB        | RLB        | SS  |
| ---------------- | ---------------------------------------- | ---:| --- | ---------- | ---------- | --- |
| ``"broadcast"``  | Broadcast policy                         | All | yes | no         | no         | no  |
| ``"failover"``   | Failover policy                          |   1 | yes | no         | no         | no  |
| ``"hash"``       | Transmit load balancing by hash          | All | yes | Hash       | no         | no  |
| ``"lacp"``       | IEEE 802.3ad dynamic link aggregation    | All | yes | Hash       | Hash       | yes |
| ``"roundrobin"`` | Round-robin load balancing               | All | yes | Sequential | no         | no  |
| ``"tlbd"``       | Adaptive transmit load balancing by load | All | yes | Load       | no         | no  |
| ``"tlbh"``       | Adaptive transmit load balancing by hash | All | yes | Hash       | no         | no  |
| ``"alb"``        | Adaptive load balancing                  | All | yes | Load       | Sequential | no  |

### ``bond.slaves`` (**str[]**, _mandatory_, bond)
#### Description
The mandatory key ``bond.slaves`` allows to specify the list of
interfaces to configure as slaves of a bond interface. This key is
mandatory, since a bond interface without slaves would be useless.
Each list item represents a single slave interface in the form of a
string matching the value of its ``uid`` key.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                               |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Invalid_                                                                                               |
| **freebsd_rc** | Set   | One ``laggport <slave_device>`` option per slave interface in ``create_args_<bond_device>`` rc variable |
| **ifupdown**   | Unset | _Invalid_                                                                                               |
| **ifupdown**   | Set   | ``bond_slaves <device[]>`` option in ``inet manual`` stanza                                             |

#### Expected values
This key must map to a list of valid interface uids. Only _adapter_
interfaces are supported as bond slaves.

### ``bridge.index`` (**int**, _mandatory_, bond)
#### Description
The mandatory key ``bridge.index`` allows to specify a literal index
number for a bridge interface. The role needs this number to build the
dynamic device name of the interface.

#### Support status
This key is only used internally by the role, and is supported in every
context.

#### Expected values
Any integer in the range 0 - 2048.

### ``bridge.members`` (**str[]**, _optional_, bridge)
#### Description
The optional key ``bridge.members`` allows to specify a list of
interfaces to configure as members of a bridge interface. If this key
maps to an empty list (default), the bridge shall have no member.
Each list item represents a single slave interface in the form of a
string matching the value of its ``uid`` key.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                              |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------ |
| **freebsd_rc** | Unset | _Nothing_                                                                                              |
| **freebsd_rc** | Set   | One ``addm <member_device>`` option per slave interface in ``create_args_<bridge_device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                                              |
| **ifupdown**   | Set   | ``bridge_ports <device[]>`` option in ``inet manual`` stanza                                           |

#### Expected values
This key must map to a list of valid interface uids, or an empty list.

### ``bridge.stp.enabled`` (**bool**, _optional_, bridge)
#### Description
The optional ``bridge.stp.enabled`` key controls whether the Spanning
Tree Protocol should be enabled on the configured bridge interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                             |
| -------------- | --------- | ------------------------------------------------------------------------------------- |
| **freebsd_rc** | ``false`` | ``-stp`` option for each bridge member in ``create_args_<bridge_device>`` rc variable |
| **freebsd_rc** | ``true``  | ``stp`` option for each bridge_member in ``create_args_<bridge_device>`` rc variable  |
| **ifupdown**   | ``false`` | ``bridge_stp off`` option in ``inet manual`` stanza                                   |
| **ifupdown**   | ``true``  | ``bridge_stp on`` option in ``inet manual`` stanza                                    |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | ``true``      |

#### Expected values
This key accepts the following values:

| Value     | Description                         |
| --------- | ----------------------------------- |
| ``false`` | Disable STP on the bridge interface |
| ``true``  | Enable STP on the bridge interface  |

### ``bridge.stp.fwd_delay`` (**int**, _optional_, bridge)
#### Description
The optional ``bridge.stp.fwd_delay`` sets the delay in seconds after
which the Spanning Tree Protocol will forward traffic on bridge member
interfaces.

> **Note**
>
> This setting has no effect if ``bridge.stp.enabled`` is set to
> ``false``

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                  |
| -------------- | ----- | -------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                  |
| **freebsd_rc** | Set   | ``fwddelay <delay>`` option in ``create_args_<bridge_device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                  |
| **ifupdown**   | Set   | ``bridge_fd <value>`` option in ``inet manual`` stanza                     |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``15``        |
| **FreeBSD** | ``15``        |

#### Expected values
This key accepts an integer value greater than 0. Accepted value ranges
are not the same on all platforms:

| Platform | Min value                   | Max value                    |
| -------- | --------------------------- | ---------------------------- |
| Debian   | _Unknown_ (we assume ``1``) | _Unknown_ (we assume ``30``) |

### ``bridge.stp.hello_time`` (**int**, _optional_, bridge)
#### Description
The optional ``bridge.stp.hello_time`` sets the delay in seconds
between each Spanning Tree Protocol configuration message.

> **Note**
>
> This setting has no effect if ``bridge.stp.enabled`` is set to
> ``false``

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                   |
| -------------- | ----- | --------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                   |
| **freebsd_rc** | Set   | ``hellotime <delay>`` option in ``create_args_<bridge_device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                   |
| **ifupdown**   | Set   | ``bridge_hello <value>`` option in ``inet manual`` stanza                   |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``2``         |
| **FreeBSD** | ``2``         |

#### Expected values
This key accepts an integer value greater than 0. Accepted value ranges
are not the same on every platform:

| Platform   | Min value                   | Max value                   |
| ---------- | --------------------------- | --------------------------- |
| **Debian** | _Unknown_ (we assume ``1``) | _Unknown_ (we assume ``2``) |

### ``bridge.stp.priority`` (**int**, _optional_, bridge)
#### Description
The optional ``bridge.stp.priority`` key sets the priority of the bridge
interface within the Spanning Tree.

> **Note**
>
> This setting has no effect if ``bridge.stp.enabled`` is set to
> ``false``

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                     |
| -------------- | ----- | ----------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                     |
| **freebsd_rc** | Set   | ``priority <priority>`` option in ``create_args_<bridge_device>`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                     |
| **ifupdown**   | Set   | ``bridge_bridgeprio <priority>`` option in ``inet manual`` stanza             |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``32768``     |
| **FreeBSD** | ``32768``     |

#### Expected values
This key accepts an integer value. Allowed ranges are platform
dependent:

| Platform    | Minimal value | Maximal value |
| ----------- | ------------- | ------------- |
| **Debian**  | ``0``         | ``65535``     |
| **FreeBSD** | ``0``         | ``61440``     |

### ``bridge.stp.variant`` (**str**, _optional_, bridge)
#### Description
The optional ``bridge.stp.variant`` key allows to define the variant
of the Spanning Tree Protocol to use with a bridge interface.

> **Note**
>
> This setting has no effect if ``bridge.stp.enabled`` is set to
> ``false``

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                  |
| -------------- | ----- | -------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                  |
| **freebsd_rc** | Set   | ``proto (stp|rstp)`` option in ``create_args_<bridge_device>`` rc variable |
| **ifupdown**   | Any   | _Unsupported_                                                              |

> **Note**
>
> Linux bridge interfaces only support the classic STP variant. As a
> consequence setting is ignored on all Linux platforms.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``classic``   |
| **FreeBSD** | ``rapid``     |

#### Expected values
This key accepts one of the following ``str`` values:

| Value       | Description              |
| ----------- | ------------------------ |
| ``classic`` | Use classic STP protocol |
| ``rapid``   | Use the RSTP protocol    |

### ``vlan.index`` (**int**, _mandatory_, vlan)
#### Description
The mandatory key ``vlan.index`` allows to specify the literal index
number of a vlan interface. The role needs this number to build the
dynamic device name of the interface. The vLAN ID bound to the
interface will also be derived from this value.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                           |
| -------------- | ----- | ------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Invalid_                                                           |
| **freebsd_rc** | Set   | ``vlan <vlan_index>`` option in ``create_args_<vlan_dev>`` variable |
| **ifupdown**   | Unset | _Invalid_                                                           |
| **ifupdown**   | Set   | ``iface <vlan_device>.<vlan_index> inet manual`` stanza             |

#### Expected values
Any integer in the range 0 - 4096.

### ``vlan.parent`` (**str**, _mandatory_, vlan)
#### Description
The mandatory ``vlan.parent`` key holds a reference to the parent
interface of a vlan interface, in the form of a string matching the
value of the ``uid`` key of the parent interface.

#### Support status
This setting will be used internally to retrieve the device name of the
parent of the vlan interface. The following table shows how this setting
shall be rendered with each network framework:

| Framework      | Value | Rendering                                                                                |
| -------------- | ----- | ---------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Invalid_                                                                                |
| **freebsd_rc** | Set   | ``vlandev <parent_device>`` option in ``create_args_<vlan_device>`` variable             |
| **ifupdown**   | Unset | _Invalid_                                                                                |
| **ifupdown**   | Set   | ``vlan-raw-device <parent_device>`` option in ``iface <vlan_device> inet manual`` stanza |

#### Expected values
This key must map to a valid interface uid representing the parent
interface.

### ``ethernet.address`` (**str**, _optional_, all types)
#### Description
The optional ``ethernet.address`` key allows to specify a literal
hardware address for the represented interface, thus enabling MAC
address spoofing.

#### Support status
The following table shows how this setting shall be rendered on each
network framework:

| Framework      | Value | Rendering                                                        |
| -------------- | ----- | ---------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                        |
| **freebsd_rc** | Set   | ``link address <address>`` option in ``ifconfig_<dev>`` variable |
| **ifupdown**   | Unset | _Nothing_                                                        |
| **ifupdown**   | Set   | ``hwaddress <address>`` option in ``inet manual`` stanza         |

#### Expected values
This key accepts an arbitrary MAC address formatted as a EUI-48 string.

### ``ethernet.mtu`` (**int**, _optional_, all types)
#### Description
The optional ``ethernet.mtu`` key allows to specify a literal value
for the Maximum Transmission Unit of the represented interface.

#### Support status
The following table shows how this setting shall be rendered on each
network framework:

| Framework      | Value | Rendering                                           |
| -------------- | ----- | --------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                           |
| **freebsd_rc** | Set   | ``mtu <mtu>`` option in ``ifconfig_<dev>`` variable |
| **ifupdown**   | Unset | _Nothing_                                           |
| **ifupdown**   | Set   | ``mtu <mtu>`` option in ``inet manual`` stanza      |

#### Expected values
This key accepts any integer value greather than 0.

### ``ethernet.randomized`` (**bool**, _optional_, all types)
#### Description
The optional ``ethernet.randomized`` key allows to enable hardware
address randomization for the represented interface. If enabled, a
random hardware address shall be generated and replace the default one.

#### Support status
The following table shows how this setting shall be rendered on each
network framework:

| Framework      | Value     | Rendering                                                     |
| -------------- | --------- | ------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                     |
| **freebsd_rc** | ``false`` | _Nothing_                                                     |
| **freebsd_rc** | ``true``  | ``link address random`` option in ``ifconfig_<dev>`` variable |
| **ifupdown**   | ``null``  | _Nothing_                                                     |
| **ifupdown**   | ``false`` | _Nothing_                                                     |
| **ifupdown**   | ``true``  | ``hwaddress random`` option in ``inet manual`` stanza         |

#### Expected values
This key accepts the following values:

| Value     | Description                                |
| --------- | ------------------------------------------ |
| ``false`` | Do not generate a random hardware address  |
| ``true``  | Generate and use a random hardware address |

### ``inet4.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet4.enabled`` key controls whether the IPv4 protocol
stack should be enabled on the represented interface. Disabling the
entire IPv4 protocol stack is not supported by every platform, and
the role shall do its best to get close to it.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                     |
| -------------- | --------- | ----------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                                     |
| **freebsd_rc** | ``false`` | ``inet -arp`` option in ``ifconfig_<dev>`` variable                           |
| **freebsd_rc** | ``true``  | _Nothing_                                                                     |
| **ifupdown**   | ``null``  | _Nothing_                                                                     |
| **ifupdown**   | ``false`` | ``post-up ip link set dev <device> arp off`` option in ``inet manual`` stanza |
| **ifupdown**   | ``true``  | _Nothing_                                                                     |

> **Note**
>
> On _ifupdown_, setting this key to ``false`` will prevent the role to
> generate ``inet`` stanzas, thus letting IPv4 unconfigured on the
> target interface. However, a default ``inet`` stanza will still be
> present, as this is the only way for us to disable ARP.

#### Expected values
This key accepts the following values:

| Value     | Description                     |
| --------- | ------------------------------- |
| ``false`` | Disable the IPv4 protocol stack |
| ``true``  | Enable the IPv4 protocol stack  |

### ``inet4.arp.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet4.arp.enabled`` key controls whether the ARP protocol
should be enabled on the represented interface. Note that disabling ARP
shall break the whole IPv4 stack.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                     |
| -------------- | --------- | ----------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                                     |
| **freebsd_rc** | ``false`` | ``inet -arp`` option in ``ifconfig_<dev>`` variable                           |
| **freebsd_rc** | ``true``  | ``inet arp`` option in ``ifconfig_<dev>`` variable                            |
| **ifupdown**   | ``null``  | _Nothing_                                                                     |
| **ifupdown**   | ``false`` | ``post-up ip link set dev <device> arp off`` option in ``inet manual`` stanza |
| **ifupdown**   | ``true``  | _Nothing_                                                                     |

#### Default value
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``true``      |
| **FreeBSD** | ``true``      |

#### Expected values
This key accepts the following values:

| Value     | Description              |
| --------- | ------------------------ |
| ``false`` | Disable the ARP protocol |
| ``true``  | Enable the ARP protocol  |

### ``inet4.bootp.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet4.bootp.enabled`` key controls whether the BOOTP
protocol should be used to obtain a dynamic IPv4 address on the
represented interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                            |
| -------------- | --------- | ------------------------------------ |
| **freebsd_rc** | _Any_     | _Unsupported_                        |
| **ifupdown**   | ``null``  | _Nothing_                            |
| **ifupdown**   | ``false`` | _Nothing_                            |
| **ifupdown**   | ``true``  | ``iface <device> inet bootp`` stanza |

#### Expected values
This key accepts the following values:

| Value     | Description                                                    |
| --------- | -------------------------------------------------------------- |
| ``false`` | Do not use the BOOTP protocol to obtain a dynamic IPv4 address |
| ``true``  | Use the BOOTP protocol to obtain a dynamic IPv4 address        |

### ``inet4.bootp.file`` (**str**, _optional_, all types)
#### Description
The optional ``inet4.bootp.file`` key allows to specify the name of the
boot file to request from the BOOTP server.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                         |
| -------------- | ----- | ------------------------------------------------- |
| **freebsd_rc** | _Any_ | _Unsupported_                                     |
| **ifupdown**   | Unset | _Nothing_                                         |
| **ifupdown**   | Set   | ``file <server>`` option in ``inet bootp`` stanza |

#### Expected values
This key accepts any string.

### ``inet4.bootp.server`` (**str**, _optional_, all types)
#### Description
The optional ``inet4.bootp.server`` key allows to specify the IP address
of the BOOTP server to contact, instead of relying on broadcast server
discovery.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework    | Value | Rendering                                           |
| ------------ | ----- | --------------------------------------------------- |
| **ifupdown** | _Any_ | _Unsupported_                                       |
| **ifupdown** | Unset | _Nothing_                                           |
| **ifupdown** | Set   | ``server <server>`` option in ``inet bootp`` stanza |

#### Expected values
This key accepts a string representing a valid IPv4 host address.

### ``inet4.dhcp.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet4.dhcp.enabled`` key controls whether the DHCP
protocol should be used to obtain a dynamic IPv4 address on the
represented interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                       |
| -------------- | --------- | ----------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                       |
| **freebsd_rc** | ``false`` | _Nothing_                                       |
| **freebsd_rc** | ``true``  | ``DHCP`` keyword in ``ifconfig_<dev>`` variable |
| **ifupdown**   | ``null``  | _Nothing_                                       |
| **ifupdown**   | ``false`` | _Nothing_                                       |
| **ifupdown**   | ``true``  | ``iface <device> inet dhcp`` stanza             |

> **Note**
>
> Unless specified otherwise, enabling DHCP on an interface which is
> also assigned one or several static addresses is a supported scenario.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | ``false``     |

#### Expected values
This key accepts the following values:

| Value     | Description                                                   |
| --------- | ------------------------------------------------------------- |
| ``false`` | Do not use the DHCP protocol to obtain a dynamic IPv4 address |
| ``true``  | Use the DHCP protocol to obtain a dynamic IPv4 address        |

### ``inet4.static`` (**dict[]**, _optional_, all types)
#### Description
The optional ``inet4.static`` key allows to define a list of static
IPv4 addresses to assign to the interface. Each list item is a ``dict``
structure, documented in the next section, under the name
``inet4.static[item]``.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                                       |
| -------------- | ----- | --------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                                                       |
| **freebsd_rc** | Set   | One ``inet <address> netmask <netmask>`` option per static address in ``ifconfig_<dev>[_alias<n>]`` variable(s) |
| **ifupdown**   | Unset | _Nothing_                                                                                                       |
| **ifupdown**   | Set   | One ``iface <device> inet static`` stanza per static address                                                    |

> **Notes**
>
> While _ifupdown_ supports setting both static and DHCP addresses on
> the same interface, this is not the case with _freebsd_rc_. If both
> are specified, the role will priorize DHCP over static addresses.

#### Expected values
This key maps to a list of ``dict`` data structures, each entry
representing a single static address. These structures are documented
in the next section, under the name ``inet4.static[item]``.

### ``inet4.static[item]`` (**dict**, _optional_, all types)
#### Description
The optional ``inet4.static[item]`` ``dict`` represents a single static
IPv4 address to be assigned to the interface.

#### Support status
The following table shows how this item shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                             |
| -------------- | ----- | ------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                             |
| **freebsd_rc** | Set   | ``inet <address> netmask <netmask>`` option in ``ifconfig_<dev>[_alias<n>]`` variable |
| **ifupdown**   | Unset | _Nothing_                                                                             |
| **ifupdown**   | Set   | ``iface <device> inet static`` stanza                                                 |

#### Expected values
This ``dict`` data structure may include the following keys, which are
documented as separate variables in the next sections:

| Key                 | Mandatory | Description                                                       |
| ------------------- | --------- | ----------------------------------------------------------------- |
| ``address``         | yes       | The static IPv4 address to assign                                 |
| ``length``          | yes       | The length of the network prefix of the IPv4 address to assign    |
| ``broadcast``       | no        | Use a static network broadcast address instead of the default one |
| ``gateway.address`` | no        | The IPv4 address of an optional default gateway                   |
| ``gateway.metric``  | no        | Literal metric value for the route through the default gateway    |

### ``inet4.static[item.address]`` (**str**, _mandatory_, all types)
#### Description
The mandatory ``inet4.static[item.address]`` key represents a static
IPv4 address to assign to an interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                           |
| -------------- | ----- | ------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                           |
| **freebsd_rc** | Set   | ``inet <address>`` option in ``ifconfig_<dev>[_alias<n>]`` variable |
| **ifupdown**   | Unset | _Invalid_                                                           |
| **ifupdown**   | Set   | ``address <address>`` in ``inet static`` stanza                     |

#### Expected values
This key must map to a string representing a valid IPv4 host address,
with no mention of a network mask.

### ``inet4.static[item.broadcast]`` (**str**, _mandatory_, all types)
#### Description
The mandatory ``inet4.static[item.broadcast]`` allows to supply an
arbitrary broadcast address for the subnet of the static IPv4 address
being assigned to the interface. If not set, the operating system shall
use the default broadcast address for the configured subnet, which
usually is the highest address from the subnet range.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                       |
| -------------- | ----- | ------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                       |
| **freebsd_rc** | Set   | ``inet broadcast <broadcast>`` option in ``ifconfig_<dev>[_alias<n>]`` variable |
| **ifupdown**   | Unset | _Nothing_                                                                       |
| **ifupdown**   | Set   | ``broadcast <address>`` in ``inet static`` stanza                               |

#### Expected values
This key must map to a string representing a valid IPv4 address, with
no mention of a network mask.

### ``inet4.static[item.length]`` (**int**, _mandatory_, all types)
#### Description
The mandatory ``inet4.static[item.length]`` key represents the length
of the network prefix of the static IPv4 address to assign to an
interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                   |
| -------------- | ----- | --------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                   |
| **freebsd_rc** | Set   | ``inet netmask <netmask>`` option in ``ifconfig_<dev>[_alias<n>]`` variable |
| **ifupdown**   | Unset | _Invalid_                                                                   |
| **ifupdown**   | Set   | ``netmask <netmask>`` in ``inet static`` stanza                             |

> **Note**
>
> This integer value shall be converted to dotted decimal or hexadecimal
> notations when needed.

#### Expected values
This key must map to an integer between 1 and 32.

### ``inet4.static[item.gateway.address]`` (**str**, _optional_, all types)
#### Description
The mandatory ``inet4.static[item.gateway.address]`` key allows to
specify the IPv4 address of a static default gateway for the represented
network interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                       |
| -------------- | ----- | ----------------------------------------------- |
| **freebsd_rc** | _Any_ | _Unsupported_                                   |
| **ifupdown**   | Unset | _Nothing_                                       |
| **ifupdown**   | Set   | ``gateway <netmask>`` in ``inet static`` stanza |

#### Expected values
This key must map to a string representing a valid IPv4 host address.

### ``inet4.static[item.gateway.metric]`` (**int**, _optional_, all types)
#### Description
The mandatory ``inet4.static[item.gateway.metric]`` key allows to
specify the metric value to assign to the route through the default
gateway. It only makes sense if ``inet4.static[item.gateway.address]``
is also specified.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                     |
| -------------- | ----- | --------------------------------------------- |
| **freebsd_rc** | _Any_ | _Unsupported_                                 |
| **ifupdown**   | Unset | _Nothing_                                     |
| **ifupdown**   | Set   | ``metric <metric>`` in ``inet static`` stanza |

#### Expected values
This key must map to an integer between 0 and 4,294,967,295.

### ``inet6.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.enabled`` key controls whether the IPv6 protocol
stack should be enabled on the represented interface. Setting this
key to ``false`` allows to disable IPv6 entirely on the configured
interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                                          |
| -------------- | --------- | -------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                                                          |
| **freebsd_rc** | ``false`` | ``ifdisabled`` option in ``ifconfig_<device>_inet6`` rc variable                                   |
| **freebsd_rc** | ``true``  | ``-ifdisabled`` option in ``ifconfig_<device>_inet6`` rc variable                                  |
| **ifupdown**   | ``false`` | ``post-up echo 1 > /proc/sys/net/ipv6/conf/<device>/disable_ipv6`` option in ``inet manual`` stanza |
| **ifupdown**   | ``true``  | _Nothing_                                                                                          |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``true``      |
| **FreeBSD** | ``false``     |

#### Expected values
This key accepts the following values:

| Value     | Description                                      |
| --------- | ------------------------------------------------ |
| ``false`` | Disable the IPv6 protocol stack on the interface |
| ``true``  | Enable the IPv6 protocol stack on the interface  |

### ``inet6.ndp.dad.attempts`` (**int**, _optional_, all types)
#### Description
The optional ``inet6.ndp.dad.attempts`` key define the maximum number of
times the Duplicate Address Detection mechanism will attempt to settle
the IPv6 address.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework    | Value | Rendering                                                             |
| ------------ | ----- | --------------------------------------------------------------------- |
| **ifupdown** | _Any_ | _Unsupported_                                                         |
| **ifupdown** | Unset | ``dad-attempts <default_attempts>`` option in ``inet6 manual`` stanza |
| **ifupdown** | Set   | ``dad-attempts <attempts>`` option in ``inet6 manual`` stanza         |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``60``        |
| **FreeBSD** | _Unknown_     |

#### Expected values
This key accepts any integer number greater than 0.

### ``inet6.ndp.dad.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.ndp.dad.enabled`` key controls whether the Duplicate
Address Detection mechanism, which is a feature of the IPv6 Network
Discovery Protocol, should be enabled on the configured interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                  |
| -------------- | --------- | -------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                                  |
| **freebsd_rc** | ``false`` | ``no_dad`` option in ``ifconfig_<device>_inet6`` rc variable               |
| **freebsd_rc** | ``true``  | ``-no_dad`` option in ``ifconfig_<device>_inet6`` rc variable              |
| **ifupdown**   | ``null``  | _Nothing_                                                                  |
| **ifupdown**   | ``false`` | ``dad-attempts 0`` option in ``inet6 manual`` stanza                       |
| **ifupdown**   | ``true``  | _Nothing_ or ``dad-attempts <attempts>`` option in ``inet6 manual`` stanza |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``true``      |
| **FreeBSD** | ``true``      |

#### Expected values
This key accepts the following values:

| Value     | Description                                                     |
| --------- | --------------------------------------------------------------- |
| ``false`` | Disable Duplicate Address Detection on the configured interface |
| ``true``  | Enable Duplicate Address Detection on the configured interface  |

### ``inet6.ndp.dad.interval`` (**float**, _optional_, all types)
#### Description
The optional ``inet6.ndp.dad.interval`` key define the polling interval
in seconds of the Duplicate Address Detection mechanism.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework    | Value | Rendering                                                             |
| ------------ | ----- | --------------------------------------------------------------------- |
| **ifupdown** | _Any_ | _Unsupported_                                                         |
| **ifupdown** | Unset | ``dad-interval <default_interval>`` option in ``inet6 manual`` stanza |
| **ifupdown** | Set   | ``dad-interval <interval>`` option in ``inet6 manual`` stanza         |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``0.1``       |
| **FreeBSD** | _Unknown_     |

#### Expected values
This key accepts any floating point number greater than 0.

### ``inet6.ndp.nud.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.ndp.nud.enabled`` key controls whether the
Neighbour Unreachability Detection mechanism, which is a feature of the
IPv6 Network Discovery Protocol, should be enabled on the configured
interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                  |
| -------------- | --------- | ---------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                  |
| **freebsd_rc** | ``false`` | ``-nud`` option in ``ifconfig_<device>_inet6`` rc variable |
| **freebsd_rc** | ``true``  | ``nud`` option in ``ifconfig_<device>_inet6`` rc variable  |
| **ifupdown**   | _Any_     | _Unsupported_                                              |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | _Unknown_     |
| **FreeBSD** | ``true``      |

#### Expected values
This key accepts the following values:

| Value     | Description                                                            |
| --------- | ---------------------------------------------------------------------- |
| ``false`` | Disable Neighbour Unreachability Detection on the configured interface |
| ``true``  | Enable Neighbour Unreachability Detection on the configured interface  |

### ``inet6.ndp.rtadv.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.ndp.rtadv.enabled`` key controls whether Router
Advertisement and Router Sollicitation messages are enabled on the
configured interface. Setting this key to ``false`` will effectively
disable the processing of all RA/RS messages on the interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                           |
| -------------- | --------- | ------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                           |
| **freebsd_rc** | ``false`` | ``-accept_rtadv`` option in ``ifconfig_<device>_inet6`` rc variable |
| **freebsd_rc** | ``true``  | ``accept_rtadv`` option in ``ifconfig_<device>_inet6`` rc variable  |
| **ifupdown**   | ``null``  | _Nothing_                                                           |
| **ifupdown**   | ``false`` | ``accept_ra 0`` option in ``inet6 manual`` stanza                   |
| **ifupdown**   | ``true``  | ``accept_ra (1|2)`` option in ``inet6 manual`` stanza               |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``true``      |
| **FreeBSD** | ``false``     |

#### Expected values
This key accepts the following values:

| Value     | Description                                                              |
| --------- | ------------------------------------------------------------------------ |
| ``false`` | Disable Router Advertisement / Solicitations on the configured interface |
| ``true``  | Enable Router Advertisement / Solicitations on the configured interface  |

### ``inet6.ndp.rtadv.forced`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.ndp.rtadv.forced`` key controls whether Router
Advertisement messages should be accepted on the configured interface,
even if the node if configured as an IPv6 router (forwarding enabled).
The IPv6 specifications explicitly state that IPv6 routers should not
accept router advertisement messages. This setting allows to bypass this
recommendation.

Note that this setting is only relevant when ``inet6.ndp.rtadv.allowed``
is set to ``true``.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                             |
| -------------- | --------- | ----------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                             |
| **freebsd_rc** | ``false`` | _Nothing_                                             |
| **freebsd_rc** | ``true``  | ``<device>`` in ``ipv6_cpe_wanif`` rc variable        |
| **ifupdown**   | ``false`` | ``accept_ra (0|1)`` option in ``inet6 manual`` stanza |
| **ifupdown**   | ``true``  | ``accept_ra (0|2)`` option in ``inet6 manual`` stanza |

> **Note**
>
> This setting is implemented quite differently from one platform to
> another, and enabling it may have important side-effects:
>
> - **On FreeBSD**, setting this key to ``true`` shall enable router
>   advertisement and router prefix registration on the configured
>   interface, even if inet6 forwarding is enabled. However, keep in
>   mind that **it will also disable those features on any other
>   interface where ``forced`` is not explicitly set to ``true``**.
>
> - **On Linux**, this setting is strictly bound to a single interface,
>   and shouldn't produce any side effect.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``true``      |
| **FreeBSD** | ``false``     |

#### Expected values
This key accepts the following values:

| Value     | Description                                                                                    |
| --------- | ---------------------------------------------------------------------------------------------- |
| ``false`` | Ignore router advertisement messages on the configured interface if the node is an IPv6 router |
| ``true``  | Accept router advertisement messages on the configured interface if the node is an IPv6 router |

### ``inet6.dhcp.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.dhcp.enabled`` key controls whether the DHCPv6
protocol must be enabled to acquire a dynamic IPv6 address on the
configured interface. If both this setting and ``inet6.slaac.enabled``
are set to ``true``, the role will configure stateless DHCPv6. If
``inet6.dhcp.enabled`` is set to ``true``, but ``inet6.slaac.enabled``
is set to ``false``, the role will configure stateful DHCPv6.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                   |
| -------------- | --------- | ------------------------------------------- |
| **freebsd_rc** | _any_     | _Unsupported_                               |
| **ifupdown**   | ``null``  | _Nothing_                                   |
| **ifupdown**   | ``false`` | _Nothing_                                   |
| **ifupdown**   | ``true``  | ``iface <device> inet6 (auto|dhcp)`` stanza |

> **Note**
>
> The ``freebsd_rc`` framework does not support DHCPv6 yet, because
> it is not yet included as a standard scenario in rc.conf variables,
> and would require the installation of third-party packages.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | _Unsupported_ |

#### Expected values
This key accepts the following values:

| Value     | Description                                        |
| --------- | -------------------------------------------------- |
| ``false`` | Do not use DHCPv6 to obtain a dynamic IPv6 address |
| ``true``  | Use DHCPv6 to obtain a dynamic IPv6 address        |

### ``inet6.dhcp.prefix.requested`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.prefix.requested`` key controls whether a network
prefix should be requested by the node when the DHCPv6 protocol is used
to obtain an IPv6 address on the configured interface. This setting
effectively enable or disable DHCPv6 prefix delegation

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                  |
| -------------- | --------- | -------------------------------------------------------------------------- |
| **freebsd_rc** | _any_     | _Unsupported_                                                              |
| **ifupdown**   | ``null``  | _Nothing_                                                                  |
| **ifupdown**   | ``false`` | ``request_prefix 0`` option in ``iface <device> inet6 (auto|dhcp)`` stanza |
| **ifupdown**   | ``true``  | ``request_prefix 1`` option in ``iface <device> inet6 (auto|dhcp)`` stanza |

> **Note**
>
> This setting is not supported with the ``freebsd_rc`` framework, like
> all other DHCPv6 settings.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | _Unsupported_ |

#### Expected values
This key accepts the following values:

| Value     | Description                             |
| --------- | --------------------------------------- |
| ``false`` | Do not request DHCPv6 prefix delegation |
| ``true``  | Request DHCPv6 prefix delegation        |

### ``inet6.slaac.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.slaac.enabled`` key controls whether the Static
Local Address AutoConfiguration mechanism should used to generate
IPv6 addresses from advertised network prefixes.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                                                     |
| -------------- | --------- | ------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                                                                     |
| **freebsd_rc** | ``false`` | ``-accept_rtadv`` options in ``ifconfig_<device>_ipv6`` rc variable                                           |
| **freebsd_rc** | ``true``  | _Nothing_                                                                                                     |
| **ifupdown**   | ``null``  | _Nothing_                                                                                                     |
| **ifupdown**   | ``false`` | ``post-up echo 0 > /proc/sys/net/ipv6/conf/<device>/autoconf`` option in ``iface <device> inet6 auto`` stanza |
| **ifupdown**   | ``true``  | ``iface <device> inet6 auto`` stanza                                                                          |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value                           |
| ----------- | --------------------------------------- |
| **Debian**  | ``true``                                |
| **FreeBSD** | ``true`` when ``inet6.enabled == true`` |

#### Expected values
This key accepts the following values:

| Value     | Description                                  |
| --------- | -------------------------------------------- |
| ``false`` | Do not use SLAAC on the configured interface |
| ``true``  | Use SLAAC on the configured interface        |

### ``inet6.slaac.privacy.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.slaac.autoconf.privacy.enabled`` key controls
whether the SLAAC mechanism should use RFC4041 privacy extensions to
generate the site-local or globally routable IPv6 addresses.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                      |
| -------------- | --------- | ---------------------------------------------- |
| **freebsd_rc** | _Any_     | _Unsupported_                                  |
| **ifupdown**   | ``false`` | ``privext 0`` in  in ``inet6 auto`` stanza     |
| **ifupdown**   | ``true``  | ``privext (1|2)`` in  in ``inet6 auto`` stanza |

> **Note**
>
> The ``freebsd_rc``framework does not allow to enable or disable
> RFC4041 temporary addresses on single interfaces. FreeBSD requires
> this setting to be managed globally. This is why this setting is not
> supported on this platform.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | _Unsupported_ |

#### Expected values
This key accepts the following values:

| Value     | Description                                                              |
| --------- | ------------------------------------------------------------------------ |
| ``false`` | Do not use privacy extensions to generate site-local or global addresses |
| ``true``  | Use privacy extensions to generate site-local or global addresses        |

### ``inet6.slaac.privacy.preferred`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.slaac.autoconf.privacy.preferred`` key controls
whether the operating system should prefer the use of temporary
addresses for outgoing traffic.

> **Note**
>
> This setting has no effect if either ``inet6.ndp.rtadv.enabled`` or
> ``inet6.slaac.privacy.enabled`` are set to ``false``.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                      |
| -------------- | --------- | ---------------------------------------------- |
| **freebsd_rc** | _Any_     | _Unsupported_                                  |
| **ifupdown**   | ``false`` | ``privext (0|1)`` in  in ``inet6 auto`` stanza |
| **ifupdown**   | ``true``  | ``privext (0|2)`` in  in ``inet6 auto`` stanza |

> **Note**
>
> The ``freebsd_rc``framework does not allow to enable or disable
> RFC4041 temporary addresses on single interfaces. FreeBSD requires
> this setting to be managed globally. This is why this setting is not
> supported on this platform.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``false``     |
| **FreeBSD** | _Unsupported_ |

#### Expected values
This key accepts the following values:

| Value     | Description                                                 |
| --------- | ----------------------------------------------------------- |
| ``false`` | Do not use RFC4041 temporary addresses for outgoing traffic |
| ``true``  | Use RFC4041 temporary addresses for outgoing traffic        |

### ``inet6.slaac.link_local.attempts`` (**int**, _optional_, all types)
#### Description
The optional ``inet6.slaac.linklocal.attempts`` key define the maximum
number of times the SLAAC mechanism will attempt to get a valid
link-local address.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                          |
| -------------- | ----- | ------------------------------------------------------------------ |
| **freebsd_rc** | _Any_ | _Unsupported_                                                      |
| **ifupdown**   | Unset | ``ll-attempts <default_attempts>`` option in ``inet6 auto`` stanza |
| **ifupdown**   | Set   | ``ll-attempts <attempts>`` option in ``inet6 auto`` stanza         |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``60``        |
| **FreeBSD** | _Unknown_     |

#### Expected values
This key accepts any integer number greater than 0.

### ``inet6.slaac.link_local.enabled`` (**bool**, _optional_, all types)
#### Description
The optional ``inet6.slaac.linklocal.enabled`` key defines whether the
interface should attempt to generate a link-local address as part of the
autoconfiguration process.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                            |
| -------------- | --------- | -------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | _Nothing_                                                            |
| **freebsd_rc** | ``false`` | ``-auto_linklocal`` option in ``ifconfig_<device>_ipv6`` rc variable |
| **freebsd_rc** | ``true``  | ``auto_linklocal`` option in ``ifconfig_<device>_ipv6`` rc variable  |
| **ifupdown**   | _Any_     | _Unsupported_                                                        |

> **Note**
>
> Disabling link-local address autoconfiguration seems to be impossible
> on Linux platforms without relying on ugly hacks. As a consequence,
> this scenario is not supported by the role.

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value                           |
| ----------- | --------------------------------------- |
| **Debian**  | ``true``                                |
| **FreeBSD** | ``true`` when ``inet6.enabled == true`` |

#### Expected values
This key accepts a nullable boolean value:

| Value     | Description                                                                      |
| --------- | -------------------------------------------------------------------------------- |
| ``false`` | Disable automatic generation of a link-local address on the configured interface |
| ``true``  | Enable automatic generation of a link-local address on the configured interface  |

### ``inet6.slaac.link_local.interval`` (**float**, _optional_, all types)
#### Description
The optional ``inet6.slaac.linklocal.interval`` key define the polling
interval in seconds of the SLAAC link-local address generation
mechanism.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                          |
| -------------- | ----- | ------------------------------------------------------------------ |
| **freebsd_rc** | _Any_ | _Unsupported_                                                      |
| **ifupdown**   | Unset | ``ll-interval <default_interval>`` option in ``inet6 auto`` stanza |
| **ifupdown**   | Set   | ``ll-interval <interval>`` option in ``inet6 auto`` stanza         |

#### Default values
The following table lists default values applied by supported platforms
when this setting remains undefined:

| Platform    | Default value |
| ----------- | ------------- |
| **Debian**  | ``0.1``       |
| **FreeBSD** | _Unknown_     |

#### Expected values
This key accepts any floating point number greater than 0.

### ``inet6.static`` (**dict[]**, _optional_, all types)
#### Description
The optional ``inet6.static`` key allows to define a list of static
IPv6 addresses to assign to the interface. Each list item is a ``dict``
structure, documented in the next section, under the name
``inet4.static[item]``.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                                                                        |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| **freebsd_rc** | Unset | _Nothing_                                                                                                                                        |
| **freebsd_rc** | Set   | One ``inet6 <address> prefixlen <prefix_length>`` option per static address in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                                                                                        |
| **ifupdown**   | Set   | One ``iface <device> inet6 static`` stanza per static address                                                                                    |

#### Expected values
This key maps to a list of ``dict`` data structures, each entry
representing a single static address. These structures are documented
in the next section, under the name ``inet6.static[item]``.

### ``inet6.static[item]`` (**dict**, _optional_, all types)
#### Description
The optional ``inet4.static[item]`` ``dict`` represents a single static
IPv6 address to be assigned to the interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                                                 |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Nothing_                                                                                                                 |
| **freebsd_rc** | Set   | ``inet6 <address> prefixlen <prefix_length>`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable |
| **ifupdown**   | Unset | _Nothing_                                                                                                                 |
| **ifupdown**   | Set   | ``iface <device> inet6 static`` stanza                                                                                    |

#### Expected values
This ``dict`` data structure may include the following keys, which are
documented as separate variables in the next sections:

| Key                 | Mandatory | Description                                                    |
| ------------------- | --------- | -------------------------------------------------------------- |
| ``address``         | yes       | The static IPv6 address to assign                              |
| ``length``          | yes       | The length of the network prefix of the IPv6 address to assign |
| ``anycast``         | no        | Whether the static address is an anycast address               |
| ``gateway.address`` | no        | The IPv6 address of an optional default gateway                |
| ``gateway.metric``  | no        | Literal metric value for the route through the default gateway |

### ``inet6.static[item.address]`` (**str**, _mandatory_, all types)
#### Description
The mandatory ``inet6.static[item.address]`` key represents a static
IPv6 address to assign to an interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                                                 |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Invalid_                                                                                                                 |
| **freebsd_rc** | Set   | ``inet6 <address> prefixlen <prefix_length>`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable |
| **ifupdown**   | Unset | _Invalid_                                                                                                                 |
| **ifupdown**   | Set   | ``address <address>`` in ``inet6 static`` stanza                                                                          |

#### Expected values
This key must map to a string representing a valid IPv6 host address,
with no mention of a network mask.

### ``inet6.static[item.anycast]`` (**str**, _optional_, all types)
#### Description
The optional ``inet6.static[item.anycast]`` key defines whether a
static IPv6 address is an anycast address.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value     | Rendering                                                                                                                         |
| -------------- | --------- | --------------------------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | ``null``  | ``inet6 <address> prefixlen <prefix_length>`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable         |
| **freebsd_rc** | ``false`` | ``inet6 <address> prefixlen <prefix_length>`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable         |
| **freebsd_rc** | ``true``  | ``inet6 <address> prefixlen <prefix_length> anycast`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable |
| **ifupdown**   | _Any_     | _Unsupported_                                                                                                                     |

#### Expected values
This key must map to a nullable boolean value:

| Value     | Description                                             |
| --------- | ------------------------------------------------------- |
| ``false`` | Configure the static IPv6 address as a unicast address  |
| ``true``  | Configure the static IPv6 address as an anycast address |

### ``inet6.static[item.length]`` (**int**, _mandatory_, all types)
#### Description
The mandatory ``inet6.static[item.length]`` key represents the length
of the network prefix of the static IPv6 address to assign to an
interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework      | Value | Rendering                                                                                                                 |
| -------------- | ----- | ------------------------------------------------------------------------------------------------------------------------- |
| **freebsd_rc** | Unset | _Invalid_                                                                                                                 |
| **freebsd_rc** | Set   | ``inet6 <address> prefixlen <prefix_length>`` option in ``ifconfig_<dev>_ipv6`` or ``ifconfig_<dev>_aliases`` rc variable |
| **ifupdown**   | Unset | _Invalid_                                                                                                                 |
| **ifupdown**   | Set   | ``netmask <netmask>`` in ``inet6 static`` stanza                                                                          |

#### Expected values
This key must map to an integer between 1 and 128.

### ``inet6.static[item.gateway.address]`` (**str**, _optional_, all types)
#### Description
The mandatory ``inet6.static[item.gateway.address]`` key allows to
specify the IPv6 address of a static default gateway for the represented
network interface.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework    | Value | Rendering                                        |
| ------------ | ----- | ------------------------------------------------ |
| **ifupdown** | Unset | _Nothing_                                        |
| **ifupdown** | Set   | ``gateway <netmask>`` in ``inet6 static`` stanza |

#### Expected values
This key must map to a string representing a valid IPv6 host address.
If the supplied address is within the link-local scope, the role shall
automatically add a zone index as a reference to the owning interface.

### ``inet6.static[item.gateway.metric]`` (**int**, _optional_, all types)
#### Description
The mandatory ``inet6.static[item.gateway.metric]`` key allows to
specify the metric value to assign to the route through the default
gateway. It only makes sense if ``inet6.static[item.gateway.address]``
is also specified.

#### Support status
The following table shows how this setting shall be rendered with each
network framework:

| Framework    | Value | Rendering                                      |
| ------------ | ----- | ---------------------------------------------- |
| **ifupdown** | Unset | _Nothing_                                      |
| **ifupdown** | Set   | ``metric <metric>`` in ``inet6 static`` stanza |

#### Expected values
This key must map to an integer between 0 and 4,294,967,295.
